# bearing-teststand



## About
LabVIEW VI used for the ME 421 Shaft project Bearing Tester.

Collects accelerometer data and RPM data versus time. Displays an FFT of the data too.